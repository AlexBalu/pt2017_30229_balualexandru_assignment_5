import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MonitoredData {
private String startTime;
private String endTime;
private String activity;

public MonitoredData(String startTime,String endTime, String activity)
{
	this.startTime = startTime;
	this.endTime = endTime;
	this.activity = activity;
}
public MonitoredData(String mdStr)
{
	startTime = mdStr.split("\t")[0].trim();
	endTime = mdStr.substring(21).split("\t")[0].trim();
    activity = mdStr.substring(42).split(" ")[0].trim();
}


public String getStartTime() {
	return startTime;
}
public void setStartTime(String startTime) {
	this.startTime = startTime;
}

public String getEndTime() {
	return endTime;
}
public void setEndTime(String endTime) {
	this.endTime = endTime;
}

public String getActivity() {
	return activity;
}

public void setActiviy(String activity) {
	this.activity = activity;
}

public String getStartDay()
{
	return this.startTime.split(" ")[0].trim();
}

public float cateOreDureaza()
{	//2011-11-28 02:27:59

	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:m:s", Locale.ENGLISH);
	Date date1,date2;
	
	try {
		date1 = format.parse(startTime);
		date2 = format.parse(endTime);
		return (date2.getTime() - date1.getTime())/1000/3600;
		
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return 0;
	
}

public float cateMinuteDureaza()
{	//2011-11-28 02:27:59

	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:m:s", Locale.ENGLISH);
	Date date1,date2;
	
	try {
		date1 = format.parse(startTime);
		date2 = format.parse(endTime);
		return (date2.getTime() - date1.getTime())/1000/60;
		
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return 0;
	
}

}
