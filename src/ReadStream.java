import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JOptionPane;

public class ReadStream {
List<MonitoredData> lista;
String numeStream;

public ReadStream(String numeStream)
{
	this.numeStream = numeStream;
	lista = new LinkedList<MonitoredData>();	
}

public void citireFisier()
{
	try (Stream<String> stream = Files.lines(Paths.get(numeStream))) {

		stream.forEach(i-> lista.add(new MonitoredData(i)));
	} catch (IOException e) {
		e.printStackTrace();
	}
}

public void numarareZile()
{
	List<String> zile = lista.stream().map(obiectMonitoredData -> obiectMonitoredData.getStartTime().split(" ")[0]).collect(Collectors.toList());
	zile.addAll(lista.stream().map(lista -> lista.getEndTime().split(" ")[0]).collect(Collectors.toList()));
	JOptionPane.showMessageDialog(null,"Numarul de zile " + zile.stream().distinct().count());
}

public void numarareActivitatiIdentice()
{
	Map<String,Integer> lm = new LinkedHashMap<>();
	lista.forEach(elementLista -> lm.put(elementLista.getActivity(), (int) lista.stream().filter(obiectPtActivitate -> (obiectPtActivitate.getActivity()).equals(elementLista.getActivity()) == true).count()));
//serializare
	try{
		    File fileOne=new File("ActivitatiSiNr.ser");
		    FileOutputStream fos=new FileOutputStream(fileOne);
		        ObjectOutputStream oos=new ObjectOutputStream(fos);

		        oos.writeObject(lm);
		        oos.flush();
		        oos.close();
		        fos.close();
		    }catch(Exception e){}
//tiparire
	 try{
		    PrintWriter writer = new PrintWriter("ActivitatiSiNr", "UTF-8");
		    writer.println(lm);
		    writer.close();
	 } catch (IOException e) {
		   System.out.println(e.getMessage());
		}	
}
public List<MonitoredData> getLm()
{
	return this.lista;
}

public  Map<String,Integer> getMapMic(String ziua)
{
	Map<String,Integer> mapMic = new LinkedHashMap<>();
	lista.stream()
	   .filter( m -> m.getStartDay().equals(ziua))
	   .forEach( e->mapMic.put(e.getActivity(), (int)lista.stream().filter( a-> a.getStartDay().equals(ziua) && a.getActivity().equals( e.getActivity()) ).count() ));
	
	return mapMic;
}
public void numarareActivitatiIdenticePeZile()
{
	Map<String, Map<String, Integer>>  mapMare = new  LinkedHashMap<String, Map<String, Integer>>();
	
	lista.stream().forEach(
			element-> mapMare.put(element.getStartDay(), this.getMapMic(element.getStartDay()))); 
	
	
	//serializare
		try{
			    File fileOne=new File("ActivitatiPeZile.ser");
			    FileOutputStream fos=new FileOutputStream(fileOne);
			        ObjectOutputStream oos=new ObjectOutputStream(fos);

			        oos.writeObject(mapMare);
			        oos.flush();
			        oos.close();
			        fos.close();
			    }catch(Exception e){}
	//tiparire
		 try{
			    PrintWriter writer = new PrintWriter("ActivitatiPeZile", "UTF-8");
			    writer.println(mapMare);
			    writer.close();
		 } catch (IOException e) {
			   System.out.println(e.getMessage());
			}	
}
   


public float durataTotalaPerActivitate(String activitate)
{
	return lista.stream().filter(element->element.getActivity().equals(activitate)).reduce(0, (sumaTotala, element) -> sumaTotala += (int)element.cateOreDureaza(),(s1, s2) -> s1 + s2);
}
public void durataFiecareActivitate()
{
	Map<String, Float> mapDurate = new LinkedHashMap<String, Float>();	
	
	lista.stream().filter(element -> durataTotalaPerActivitate(element.getActivity())>10)
	  .forEach( el2->mapDurate.put(el2.getActivity(),durataTotalaPerActivitate(el2.getActivity())));
//serializare
		try{
			    File fileOne=new File("Durate.ser");
			    FileOutputStream fos=new FileOutputStream(fileOne);
			        ObjectOutputStream oos=new ObjectOutputStream(fos);

			        oos.writeObject(mapDurate);
			        oos.flush();
			        oos.close();
			        fos.close();
			    }catch(Exception e){}
//tiparire
		 try{
			    PrintWriter writer = new PrintWriter("Durate", "UTF-8");
			    writer.println(mapDurate);
			    writer.close();
		 } catch (IOException e) {
			   System.out.println(e.getMessage());
			}	
}


//problema 5
public boolean ePeste90(String activitate)
{
	int aparitiiMaiMici5 = (int)lista.stream().filter(element-> element.getActivity().equals(activitate) && element.cateMinuteDureaza()<5 ).count();
	int aparitiiTotale = (int)lista.stream().filter(element-> element.getActivity().equals(activitate)).count();
	if( aparitiiMaiMici5*100 / aparitiiTotale > 90 )
		return true;
	else return false;
}

public void aparitii5Min()
{
	 
	 List<String> listStr  = lista.stream().filter(element-> ePeste90(element.getActivity())).map(element->element.getActivity()).distinct().collect(Collectors.toList());
	
	  System.out.println(listStr);
	//serializare
			try{
				    File fileOne=new File("Activitati5.ser");
				    FileOutputStream fos=new FileOutputStream(fileOne);
				        ObjectOutputStream oos=new ObjectOutputStream(fos);

				        oos.writeObject(listStr);
				        oos.flush();
				        oos.close();
				        fos.close();
				    }catch(Exception e){}
	//tiparire
			 try{
				    PrintWriter writer = new PrintWriter("Activitati5", "UTF-8");
				    writer.println(listStr);
				    writer.close();
			 } catch (IOException e) {
				   System.out.println(e.getMessage());
				}	
}


}
